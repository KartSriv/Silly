#!/usr/bin/python
"""
    This program "Silly.py" is a basic A.I built to do simple tasks.
    Such as: Google, Github, Voice, etc..
    Author: Karthik Srivijay
    Launguage: Python 2.x
    Version: 4.0.1
"""

# Importing all modules required
import os
import subprocess as sp
import webbrowser

# Opening name.txt
FH = open("name.txt")
full_text = FH.read()
all_lines = full_text.split('\n')
for line in all_lines :
      name=line
FH.close()

# Checking if Silly is configured

if name=="Not set yet":
    # Checking if Silly is configured
    newusr=raw_input("Hey there! I am Silly and I wanted to know who your: ")
    target = open("name.txt", 'w')
    target.truncate()
    target.write(newusr)
    target.close()

# Configured Silly

FH = open("name.txt")
full_text = FH.read()
all_lines = full_text.split('\n')
for line in all_lines:
    name = line
FH.close()

# First Name
FullName = name.split()
FirstName=FullName[0]
# Last Name
LastName=FullName[1]
# Change Name to First Name
if name!=FirstName:
    name=FirstName

# Starting Silly
while True: # Making a infinity loop
    usrinput = raw_input("Hello, " + name + ". Want any help? ")
    usrinput=usrinput.lower() # Coverting Userinput LowerCase
    if usrinput == "exit" or usrinput == "Exit" or usrinput == "Exit":
        print "Good bye "+name+"! Had a good time with you."
        break
    elif usrinput == "What's the date?" or usrinput == "What is the date?" or usrinput == "date":
        f = os.popen('date')
        now = f.read()
        print "Today is ", now
    elif usrinput=="Add project" or usrinput=="Make a new project":
        usrinput=raw_input(name+" should I put this project on our server? ")
        if usrinput=="Yes" or usrinput=="Y" or usrinput=="yes" or usrinput=="y" or usrinput=="Sure" or usrinput=="of course" or usrinput=="Of course":
            print "Hello"
    elif usrinput=="Test" or usrinput=="test":
        print FirstName
        print LastName